# Eccelerators Samples
This project contains several examples of the use of Eccellerators' HxS hardware-software interface.

<a href="https://eccelerators.com"><img src="resources/eccelerators.svg" width="400"></a>

## What is HxS
HxS is where Hardware meets Software
For more information visit: https://eccelerators.com/hxs

## HTML
Generated HTML outut at: <a href="https://eccelerators-hxs.gitlab.io/samples">link</a>

## Used HxS Features inside inside the samples
- async
  - Clock Domain Crossing
  - Read Transparent
  - Write Register
  - Register overlay
- pwm
  - Simple read / write registers
  - Reuse of Objects
  - Overwrite keys
