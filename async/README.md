# Eccelerators Sample : Async
This is an Sample project for [HxS](https://eccelerators.com/hxs).

## Used HxS Features
- Clock Domain Crossing
- Read Transparent
- Write Register
- Register overlay

## Info
This example shows you how to use different clock domains in conjunction with the register description language HxS.  
For a better illustration of this, a motor control is simulated here.  
The control register enables the motor to be switched on and off.  
Via a second register the speed of the motor can be set.  
The special feature here is that the current speed can be read back via the same address.

## How to use
To generate all output files over the Makefile:

```bash
make all
```
