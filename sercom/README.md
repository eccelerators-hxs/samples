# Eccelerators Sample : SERCOM
This is an Sample project for [HxS](https://eccelerators.com/hxs).

## Used HxS Features
- Selects, more than one register at same address
- additional soft reset
- typical mix of registers and bits

## Info
This example shows a typical mix of functions used to model a UART.  

## How to use
To generate all output files using the Makefile:

```bash
make all
```
