# Eccelerators Sample : Generic
This is an Sample project for [HxS](https://eccelerators.com/hxs).

## Used HxS Features
- Generics of reset

## Info
An basic Version example that use different variants of contant behaviors.

## How to use
To generate all output files over the Makefile:

```bash
make all
```
