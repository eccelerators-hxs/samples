Welcome to generics's documentation!
====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   src-gen/Eccelerators.Samples.ifc-composite


Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
