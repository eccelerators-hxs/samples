# Eccelerators Sample : Const
This is an Sample project for [HxS](https://eccelerators.com/hxs).

## Used HxS Features
- Constant Values
- Constant Values with Generic

## Info
An basic Version example that use different variants of constant behaviors.

## How to use
To generate all output files over the Makefile:

```bash
make all
```
