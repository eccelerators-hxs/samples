# Eccelerators Sample : PWM
This is an Sample project for [HxS](https://eccelerators.com/hxs).

## Used HxS Features
- Simple read / write registers
- Reuse of Objects
- Overwrite keys

## Info
This example shows how easy it is to use an existing object multiple times.  
For illustration a PWM control was simulated here.  
Each of the two LEDs as RGB variant can be controlled individually.  
On the one hand the brightness can be adjusted.  
Also the complete switching on and off was realized here via a control register.

## How to use
To generate all output files over the Makefile:

```bash
make all
```
