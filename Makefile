HTML_THEME ?= 'haiku'
_types := docs vhdl c all html clean

.PHONY: $(_types)
$(_types)::
	$(MAKE) -C async $@
	$(MAKE) -C pwm $@
	$(MAKE) -C sercom $@

html::
	@mkdir -p                    ./public/$(HTML_THEME)
	@cp -rf  async/src-gen/html  ./public/$(HTML_THEME)/async
	@cp -rf    pwm/src-gen/html  ./public/$(HTML_THEME)/pwm
	@cp -rf sercom/src-gen/html  ./public/$(HTML_THEME)/sercom
